#!/usr/bin/env node

const commander = require("commander"),
  chalk = require("chalk"),
  fs = require("fs"),
  lib = require("../lib");

commander
  .command("create <string>")
  .description("Create a new migration file")
  .option("-c, --config <path>", "Set path to config file", "./travel.config.json")
  .action((cmd, options) => {
    lib.create(cmd, options.config)
      .then((res) => {
        console.log(chalk.cyan("Create migration file:"), chalk.cyan.bold('"' + res + '"'));
        process.exit();
      })
      .catch((err) => {
        console.log(chalk.red(err));
        process.exit();
      });
  });

commander
  .command("up")
  .description("Migrate up till given migration")
  .option("-c, --config <path>", "Set path to config file", "./travel.config.json")
  .action((cmd, options) => {
    if (!options) {
      options = cmd;
      cmd = null;
    }
    lib.migrate("up", cmd, options.config)
      .then((res) => {
        if (res && Array.isArray(res)) {
          res.forEach(function(doneMigrationFile) {
            console.log(chalk.cyan("Up:"), chalk.cyan.bold('"' + doneMigrationFile + '"'));
          });
        }
        console.log(chalk.cyan.bold("Up successfully comleted"));
        process.exit();
      })
      .catch((err) => {
        console.log(chalk.red(err));
        process.exit();
      });
  });

commander
  .command("down")
  .description("Migrate down till given migration")
  .option("-c, --config <path>", "Set path to config file", "./travel.config.json")
  .action((cmd, options) => {
    if (!options) {
      options = cmd;
      cmd = null;
    }
    lib.migrate("down", cmd, options.config)
      .then((res) => {
        if (res && Array.isArray(res)) {
          res.forEach(function(doneMigrationFile) {
            console.log(chalk.cyan("Down:"), chalk.cyan.bold('"' + doneMigrationFile + '"'));
          });
        }
        console.log(chalk.cyan.bold("Down successfully comleted"));
        process.exit();
      })
      .catch((err) => {
        console.log(chalk.red(err));
        process.exit();
      });
  });

commander.parse(process.argv);
