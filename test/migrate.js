"use strict";

const chai = require("chai"),
      chaiFiles = require('chai-files'),
      Promise = require("bluebird"),
      fs = require("fs"),
      path = require("path"),
      mongodb = require("mongodb"),
      create = require("../lib/create"),
      migrate = require("../lib/migrate");

chai.use(chaiFiles);
const expect = chai.expect,
      file = chaiFiles.file;

const removeDir = Promise.promisify(fs.rmdir),
      readDir = Promise.promisify(fs.readdir),
      removeFile = Promise.promisify(fs.unlink),
      MongoClient = Promise.promisify(mongodb.MongoClient);

describe("Execute migration", function() {

  after(function(done) {
    var pathDirMigrations = path.join(__dirname, "migrations");
    readDir(pathDirMigrations)
      .map(function(fileName) {
        return removeFile(path.join(pathDirMigrations, fileName))
      })
      .then(function() {
        return removeDir(pathDirMigrations)
      })
      .then(function() {
        done();
      })
      .catch(function(err) {
        console.log(err);
        done();
      });
  });

  describe("Execute migration with errors", function() {

    it("should return an error about lack of configuration file", function (done) {
      migrate("up", "add-some-table", "./some_config.json")
        .catch(function (err) {
          expect(err).to.be.instanceof(Error);
          expect(err.message).to.have.string("ENOENT: no such file or directory, access");
          done();
        });
    });

    it("should return an error when connecting to DB", function (done) {
      var configFilePath = path.join(__dirname, "config_tests", "config_bad_connection_db.json");
      migrate("up", null, configFilePath)
        .catch(function (err) {
          expect(err).to.be.instanceof(Error);
          expect(err.message).to.have.string("failed to connect to server");
          done();
        });
    });

    it("should return an authentication error to the database", function (done) {
      var configFilePath = path.join(__dirname, "config_tests", "config_bad_auth_db.json");
      migrate("up", null, configFilePath)
        .catch(function (err) {
          expect(err).to.be.instanceof(Error);
          expect(err.message).to.have.string("Authentication failed");
          done();
        });
    });

    it("should return an error that the file is missing migration", function (done) {
      var configFilePath = path.join(__dirname, "config_tests", "config.json");
      var migrationFile = path.join(__dirname, "migrations", "000-add-some-table.js");
      migrate("up", migrationFile, configFilePath)
        .catch(function (err) {
          expect(err).to.be.instanceof(Error);
          expect(err.message).to.have.string("ENOENT: no such file or directory, access");
          done();
        });
    });

  });

  describe("Execute all migration", function() {

    var db, migrationFiles = [];
    var defaultConfigFilePath = path.join(__dirname, "config_tests", "config.json");
    var testData = [
      {
        config: "config-1.json",
        collection: "some_collection#1"
      }, {
        config: "config-2.json",
        collection: "some_collection#2"
      }, {
        config: "config-3.json",
        collection: "some_collection#3"
      }
    ];

    before(function(done) {
      var config = require(defaultConfigFilePath);
      Promise.reduce(testData, function(PromiseCreate, data) {
          if (!PromiseCreate) {
            PromiseCreate = Promise.resolve();
          }
          var configFilePath = path.join(__dirname, "config_tests", data.config);
          return PromiseCreate.then(function() {
            return create("add_" + data.collection, configFilePath).then(function (migrationFilePath) {
              migrationFiles.push(path.basename(migrationFilePath));
              return PromiseCreate;
            });
          });
        }, Promise.resolve())
        .then(function() {
          var login = config.db.user ? config.db.user + ":" + config.db.password + "@" : "";
          var url = "mongodb://" + login + config.db.host + ":" + config.db.port + "/" + config.db.name;
          return MongoClient.connect(url);
        })
        .then(function(connection) {
          db = connection.db(config.db.name);
          done();
        })
        .catch(function(err) {
          console.log(err);
          done();
        });
    });

    it("should create collection and be a record of the migration", function (done) {
      migrate("up", null, defaultConfigFilePath)
        .map(function (doneMigration) {
          return db.collection("migrations").find({ name: doneMigration }).toArray();
        })
        .then(function (items) {
          expect(items).to.be.an("array");
          expect(items).to.have.lengthOf(testData.length);
          items.forEach(function(item, index) {
            expect(item[0]).to.be.an("object");
            expect(item[0].name).to.contain(testData[index].collection);
          });
          return testData;
        })
        .map(function (data) {
          return db.listCollections({ name: data.collection }).toArray();
        })
        .then(function (collections) {
          expect(collections).to.be.an("array");
          expect(collections).to.have.lengthOf(testData.length);
          collections.forEach(function(collection, index) {
            expect(collection[0]).to.be.an("object");
            expect(collection[0].name).to.equal(testData[index].collection);
          });
          done();
        });
    });

    it("should delete all collection and be no record on migration", function (done) {
      migrate("down", null, defaultConfigFilePath)
        .map(function (doneMigration) {
          return db.collection("migrations").find({ name: doneMigration }).toArray();
        })
        .then(function (items) {
          expect(items).to.be.an("array");
          expect(items).to.have.lengthOf(testData.length);
          items.forEach(function(item) {
            expect(item).to.be.empty;
          });
          return testData;
        })
        .map(function (data) {
          return db.listCollections({ name: data.collection }).toArray();
        })
        .then(function (collections) {
          expect(collections).to.be.an("array");
          expect(collections).to.have.lengthOf(3);
          collections.forEach(function(collection) {
            expect(collection).to.be.empty;
          });
          done();
        });
    });

  });

  describe("Execute migration with file", function() {

    var testCollection = "some_collection";
    var migrationFile, db, configFilePath;

    before(function(done) {
      configFilePath = path.join(__dirname, "config_tests", "config.json");
      var config = require(configFilePath);
      create("add-some-table", configFilePath)
        .then(function(migrationFilePath) {
          migrationFile = path.basename(migrationFilePath);
          var login = config.db.user ? config.db.user + ":" + config.db.password + "@" : "";
          var url = "mongodb://" + login + config.db.host + ":" + config.db.port + "/" + config.db.name;
          return MongoClient.connect(url);
        })
        .then(function(connection) {
          db = connection.db(config.db.name);
          done();
        })
        .catch(function(err) {
          console.log(err);
        });
    });

    it("should create collection with migration file", function (done) {
      migrate("up", migrationFile, configFilePath)
        .then(function () {
          return db.collection("migrations").find({ name: migrationFile }).toArray();
        })
        .then(function (items) {
          expect(items).to.be.an("array");
          expect(items).to.have.lengthOf(1);
          expect(items[0]).to.be.an("object");
          expect(items[0].name).to.equal(migrationFile);
        })
        .then(function () {
          return db.listCollections({name: testCollection}).toArray();
        })
        .then(function (collections) {
          expect(collections).to.be.an("array");
          expect(collections).to.have.lengthOf(1);
          expect(collections[0]).to.be.an("object");
          expect(collections[0].name).to.equal(testCollection);
          done();
        });
    });

    it("should delete collection with migration file", function (done) {
      migrate("down", migrationFile, configFilePath)
        .then(function () {
          return db.collection("migrations").find({ name: migrationFile }).toArray();
        })
        .then(function (items) {
          expect(items).to.be.an("array");
          expect(items).to.have.lengthOf(0);
        })
        .then(function () {
          return db.listCollections({name: testCollection}).toArray();
        })
        .then(function (collections) {
          expect(collections).to.be.an("array");
          expect(collections).to.have.lengthOf(0);
          done();
        });
    });

  });

});
