"use strict";

exports.up = function(db, done) {
  return db.createCollection("some_collection#2")
    .then(function(collection) {
      collection.insertOne({ name: "John", age: 34 });
      return done;
    })
    .catch(function(err) {
      return err;
    });
};

exports.down = function(db, done) {
  return db.dropCollection("some_collection#2")
    .then(function() {
      return done;
    })
    .catch(function(err) {
      return err;
    });
};
