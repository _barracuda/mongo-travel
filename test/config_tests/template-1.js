"use strict";

exports.up = function(db, done) {
  return db.createCollection("some_collection#1")
    .then(function(collection) {
      collection.insertOne({ name: "Alex", age: 25 });
      return done;
    })
    .catch(function(err) {
      return err;
    });
};

exports.down = function(db, done) {
  return db.dropCollection("some_collection#1")
    .then(function() {
      return done;
    })
    .catch(function(err) {
      return err;
    });
};
