"use strict";

exports.up = function(db, done) {
  return db.createCollection("some_collection#3")
    .then(function(collection) {
      collection.insertOne({ name: "David", age: 28 });
      return done;
    })
    .catch(function(err) {
      return err;
    });
};

exports.down = function(db, done) {
  return db.dropCollection("some_collection#3")
    .then(function() {
      return done;
    })
    .catch(function(err) {
      return err;
    });
};
