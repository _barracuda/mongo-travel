"use strict";

const chai = require("chai"),
      chaiFiles = require('chai-files'),
      Promise = require("bluebird"),
      fs = require("fs"),
      path = require("path"),
      create = require("../lib/create");

chai.use(chaiFiles);
const expect = chai.expect,
      file = chaiFiles.file;

const removeDir = Promise.promisify(fs.rmdir),
      readDir = Promise.promisify(fs.readdir),
      removeFile = Promise.promisify(fs.unlink);

describe("Create migrate", function() {

  after(function(done) {
    var pathDirMigrations = path.join(__dirname, "migrations");
    readDir(pathDirMigrations)
      .map(function(fileName) {
        return removeFile(path.join(pathDirMigrations, fileName));
      })
      .then(function() {
        return removeDir(pathDirMigrations);
      })
      .then(function() {
        done();
      })
      .catch(function(err) {
        console.log(err);
        done();
      });
  });

  it("should return an error about lack of configuration file", function(done) {
    var configFilePath = path.join(__dirname, "config_tests", "some_config.json");
    create("add-some-table", configFilePath)
      .catch(function(err) {
        expect(err).to.be.instanceof(Error);
        expect(err.message).to.have.string("ENOENT: no such file or directory, access");
        done();
      });
  });

  it("should return an error about lack of a template file", function(done) {
    var configFilePath = path.join(__dirname, "config_tests", "config_bad_template.json");
    create("add-some-table", configFilePath)
      .catch(function(err) {
        expect(err).to.be.instanceof(Error);
        expect(err.message).to.have.string("ENOENT: no such file or directory, access");
        done();
      });
  });

  it("should return error on incorrect format of config file", function(done) {
    var configFilePath = path.join(__dirname, "config_tests", "config.js");
    create("add-some-table", configFilePath)
      .catch(function(err) {
        expect(err).to.be.instanceof(Error);
        expect(err.message).to.have.string("Incorrect format of config file, you need json");
        done();
      });
  });

  it("should be created the migration file and return its name", function(done) {
    var configFilePath = path.join(__dirname, "config_tests", "config.json");
    create("add-some-collection", configFilePath)
      .then((migrateFilePath) => {
        expect(file(migrateFilePath)).to.exist;
        expect(file(migrateFilePath)).to.be.equal(file(path.join(__dirname, "config_tests", "template.js")));
        expect(file(migrateFilePath)).to.not.be.empty;
        done();
      });
  });

});
