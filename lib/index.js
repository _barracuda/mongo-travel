"use strict";

const create = require("./create"),
      migrate = require("./migrate");

module.exports = {
  create: create,
  migrate: migrate
};


