"use strict";

const Promise = require("bluebird"),
      path = require("path"),
      fs = require("fs"),
      mongodb = require("mongodb"),
      moment = require("moment"),
      config = require("nconf"),
      defaults = require("./defaults");

const checkAccess = Promise.promisify(fs.access),
      readDir = Promise.promisify(fs.readdir),
      MongoClient = Promise.promisifyAll(mongodb.MongoClient);

const ACTION_UP = "up";
const ACTION_DOWN = "down";
var absoluteStartPath, db;

module.exports = function(migrateAction, migrationFileName, configFilePath) {
  return Promise.resolve(checkAccess(configFilePath))
    .then(function() {
      if (migrateAction !== ACTION_UP && migrateAction !== ACTION_DOWN) {
        throw new Error("Wrong action migration");
      }

      config.file(configFilePath);
      config.defaults(defaults);
      absoluteStartPath = path.dirname(configFilePath);
      config.set("dir", path.join(absoluteStartPath, config.get("dir")));

      if (migrationFileName) {
        var migrationFilePath = path.join(config.get("dir"), migrationFileName);
        return checkAccess(migrationFilePath);
      }
    })
    .then(function() {
      var login = config.get("db:user") ? config.get("db:user") + ":" + config.get("db:password") + "@" : "";
      var url = "mongodb://" + login + config.get("db:host") + ":" + config.get("db:port") + "/" + config.get("db:name");
      return MongoClient.connect(url);
    })
    .then(function(connection) {
      db = connection.db(config.get("db:name"));
      var collection = db.collection("migrations");
      var dir = path.resolve(config.get("dir"));
      var migrations = migrationFileName ? [migrationFileName] : readDir(dir);

      return Promise.resolve([ collection.find({}).toArray(), migrations ])
        .spread(function(dbMigrations, fsMigrations) {
          var dbMigrationsFormatted = dbMigrations.map(function (migrate) {
            return migrate.name;
          });

          return fsMigrations.map(function (migrate) {
            var filter;

            if (migrateAction === ACTION_UP) {
              filter = !~dbMigrationsFormatted.indexOf(migrate);
            } else {
              filter = ~dbMigrationsFormatted.indexOf(migrate);
            }

            if (filter) {
              return migrate;
            }
          });

        });
    })
    .map(function(migrationFile) {
      if (!migrationFile) {
        return;
      }

      var dir = path.resolve(config.get("dir"));
      var migrate = require(path.join(dir, migrationFile));

      if (migrateAction === ACTION_UP) {
        return migrate.up(db, Promise.resolve(migrationFile));
      }

      if (migrateAction === ACTION_DOWN) {
        return migrate.down(db, Promise.resolve(migrationFile));
      }
    })
    .then(function(migrationFiles) {
      return migrationFiles.filter(function(file) {
        return file;
      });
    })
    .then(function(doneMigrationFiles) {
      var migrationData = [];

      if (doneMigrationFiles.length > 0) {
        var collection = db.collection("migrations");

        if (migrateAction === ACTION_UP) {
          doneMigrationFiles.forEach(function(migration) {
            migrationData.push({
              name: migration,
              migration_at: moment().format("YYYY-MM-DD HH:mm:ss")
            });
          });
          return collection.insertMany(migrationData)
            .then(function() {
              return doneMigrationFiles;
            });
        }

        if (migrateAction === ACTION_DOWN) {
          return Promise.resolve(doneMigrationFiles)
            .map(function(migration) {
              return collection.deleteMany({ name: migration });
            })
            .then(function() {
              return doneMigrationFiles;
            });
        }

      }
    });
};
