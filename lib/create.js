"use strict";

const Promise = require("bluebird"),
      moment = require("moment"),
      path = require("path"),
      config = require("nconf"),
      defaults = require("./defaults"),
      fs = require("fs");

const checkAccess = Promise.promisify(fs.access),
      createDir = Promise.promisify(fs.mkdir),
      readFile = Promise.promisify(fs.readFile),
      createFile = Promise.promisify(fs.writeFile);

var absoluteStartPath, migrateFilePath;

module.exports = function(migrateName, configFilePath) {
  return Promise.resolve(checkAccess(configFilePath))
    .then(function() {
      if (path.extname(configFilePath) !== ".json") {
        throw new Error("Incorrect format of config file, you need json");
      }
      config.file(configFilePath);
      config.defaults(defaults);
      absoluteStartPath = path.dirname(configFilePath);
      config.set("dir", path.join(absoluteStartPath, config.get("dir")));
      return checkAccess(config.get("dir"))
        .catch(function(err) {
          return createDir(config.get("dir")).catch(function () {
            return;
          });
        });
    })
    .then(function(folder) {
      if (!config.get("template")) {
        config.set("template", path.join(__dirname, "template.js"));
      } else {
        config.set("template", path.join(absoluteStartPath, config.get("template")));
      }
      return checkAccess(config.get("template"));
    })
    .then(function() {
      return readFile(config.get("template"));
    })
    .then(function(template) {
      migrateFilePath = path.join(config.get("dir"), moment() + "-" + migrateName + ".js");
      return createFile(migrateFilePath, template);
    })
    .then(function() {
      return migrateFilePath;
    });
};
